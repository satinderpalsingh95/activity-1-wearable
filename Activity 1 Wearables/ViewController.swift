//
//  ViewController.swift
//  Activity 1 Wearables
//
//  Created by Satinder pal Singh on 2019-10-28.
//  Copyright © 2019 Satinder pal Singh. All rights reserved.
//

import UIKit
import WatchConnectivity

class ViewController: UIViewController, WCSessionDelegate{
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        if (WCSession.isSupported() == true) {
            nameLabel.text = "WCSupported!"
            
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        else{
            nameLabel.text = "WC NOT supported!"
        }
        
        
        
        // Do any additional setup after loading the view.
    }
    @IBAction func buttonPressed(_ sender: Any) {
        if (WCSession.default.isReachable) {
            let message = ["name":"banana","color":"yellow"] as [String : Any]
            
            WCSession.default.sendMessage(message, replyHandler: nil)
        }
    }
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        let name = message["name"] as! String
        let age = message["age"] as! String
        print("\(name) and \(age)")
        nameLabel.text = name
        ageLabel.text = age
    }
    

}

