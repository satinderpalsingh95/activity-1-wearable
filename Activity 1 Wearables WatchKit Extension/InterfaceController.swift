//
//  InterfaceController.swift
//  Activity 1 Wearables WatchKit Extension
//
//  Created by Satinder pal Singh on 2019-10-28.
//  Copyright © 2019 Satinder pal Singh. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WKInterfaceController, WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
    
    }
    
    

    @IBOutlet weak var colorLabel: WKInterfaceLabel!
    @IBOutlet weak var nameLabel: WKInterfaceLabel!
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        if (WCSession.isSupported() == true) {
            //nameLabel.setText("WC enabled")
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        else {
            nameLabel.setText("WC NOT supported!")
        }
    }
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        let name = message["name"] as! String
        let color = message["color"] as! String
        print("\(name) and \(color)")
        nameLabel.setText(name)
        colorLabel.setText(color)
        
        
    }
    
    
    @IBAction func buttonPressedWatch() {
        if (WCSession.default.isReachable) {
            let message = ["name":"Pretesh","age":"28"] as [String : Any]
            
            WCSession.default.sendMessage(message, replyHandler: nil)
        
        }
    }
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
